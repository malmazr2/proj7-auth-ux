<html>
  <head>
    <title>CIS 322 Proj6-rest</title>
  </head>
  
  <body>  
    <h1>------List All------</h1>
    <h1>Open Time</h1>
    <ul>
      <?php
       $json = file_get_contents('http://laptop-service/listAll');
       $obj = json_decode($json);
       $open_time = $obj->open_time;
      $close_time = $obj->close_time;
      
      foreach ($open_time as $l) {
      echo "<li>$l</li>";
      }
      ?>
    </ul>
    <h1>Close Time</h1>
    <ul>
      <?php
       $json = file_get_contents('http://laptop-service/listAll');
       $obj = json_decode($json);
       $open_time = $obj->open_time;
      $close_time = $obj->close_time;

      foreach ($close_time as $l) {
      echo "<li>$l</li>";
      }
      ?>
    </ul>
    <h1>------Open Only------</h1>
    <h1>Open Time</h1>
    <ul>
      <?php
       $json = file_get_contents('http://laptop-service/listOpenOnly');
       $obj = json_decode($json);
       $open_time = $obj->open_time;

      foreach ($open_time as $l) {
      echo "<li>$l</li>";
      }
      ?>
    </ul>
    <h1>------Close Only------</h1>
    <h1>Close Time</h1>
    <ul>
      <?php
       $json = file_get_contents('http://laptop-service/listCloseOnly');
       $obj = json_decode($json);
       $open_time = $obj->close_time;

      foreach ($close_time as $l) {
      echo "<li>$l</li>";
      }
      ?>
    </ul>
    <h1>------List Top 3------</h1>
    <h1>Open Time</h1>
    <ul>
      <?php                                                                                         
       $json = file_get_contents('http://laptop-service/listAll?top=3');                                  
       $obj = json_decode($json);                                                                   
       $open_time = $obj->open_time;
      $close_time = $obj->close_time;

      foreach ($open_time as $l) {
      echo "<li>$l</li>";
      }
      ?>
    </ul>
    <h1>Close Time</h1>
    <ul>
      <?php                                                                                         
       $json = file_get_contents('http://laptop-service/listAll?top=3');                                  
       $obj = json_decode($json);                                                                   
       $open_time = $obj->open_time;
      $close_time = $obj->close_time;

      foreach ($close_time as $l) {
      echo "<li>$l</li>";
      }
      ?>
    </ul>
    <h1>------List All (CSV)------</h1>
    <ul>
      <?php
       $csv = file_get_contents('http://laptop-service/listAll/csv');
       echo nl2br ($csv);
       ?>
    </ul>
    <h1>------List Open Only (CSV)------</h1>
    <ul>
      <?php
       $csv = file_get_contents('http://laptop-service/listOpenOnly/csv');
       echo nl2br ($csv);
       ?>
    </ul>
    <h1>------List Close Only (CSV)------</h1>
    <ul>
      <?php
       $csv = file_get_contents('http://laptop-service/listCloseOnly/csv');
       echo nl2br ($csv);
       ?>
    </ul>
    <h1>------List Top 3 (CSV)------</h1>
    <ul>
      <?php                                                                                         
       $csv = file_get_contents('http://laptop-service/listAll/csv?top=3');                               
       echo nl2br ($csv);                                                                           
       ?>
    </ul>
  </body>
</html>
