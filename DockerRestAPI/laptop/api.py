# Laptop Service
import pymongo
import os
import csv
import sys
import time
import random
import flask_login
from pymongo import MongoClient
from flask import Flask, request, Response, redirect, url_for, render_template, jsonify, abort, flash
from flask_restful import Resource, Api
from flask.json import JSONEncoder
from itsdangerous import (TimedJSONWebSignatureSerializer as Serializer, BadSignature, SignatureExpired)
from wtforms import StringField, PasswordField, BooleanField
from wtforms.validators import DataRequired
from flask_wtf import Form, CSRFProtect
from flask_login import LoginManager, UserMixin, login_required, login_user, logout_user
from password import hash_password, verify_password
from testToken import generate_auth_token, verify_auth_token
from passlib.apps import custom_app_context as pwd_context
from flask_httpauth import HTTPBasicAuth
auth = HTTPBasicAuth()

# Instantiate the app
app = Flask(__name__)
api = Api(app)

app.config['SECRET_KEY'] = 'Secrets arent good for your health'


# Getting the database
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb
userdb = client.users

login_manager = LoginManager()
login_manager.init_app(app)

login_manager.login_view = "login"
login_manager.login_message = "Please log in to acess this page"
login_manager.refresh_view = "reauth"

login_manager.setup_app(app)

class User():
    def __init__(self,username):
        self.username = username
    def is_authenticated(self):
        return True
    def is_active(self):
        return True
    def is_anonymous(self):
        return False
    def get_id(self):
        return self.username
        
@login_manager.user_loader
def load_user(username):
    user = userdb.users.find_one({ 'username': username })
    if not user:
        return None
    return User(user['_id'])

@auth.verify_password
def verify_password(us_or_t, password):
    user = verify_auth_token(us_or_t)
    if not user:
        user = User.query.filter_by(username = username_or_token).first()
        if not user or not user.verify_password(password):
            return False
    g.user = user
    return True

class RegistrationForm(Form):
    username = StringField('username',[DataRequired(message='Username is required!')])
    password = PasswordField('password',[DataRequired(message='Password is required!')])

class LoginForm(Form):
    username = StringField('username', validators=[DataRequired()])
    password = PasswordField('password', validators=[DataRequired()])
        

@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html')

@app.route('/api/register', methods=['GET', 'POST'])
def register():
    form = RegistrationForm(request.form)
    
    if request.method == 'POST' and form.validate_on_submit():
        username = form.username.data
        password = form.password.data

        isUser = userdb.users.find_one({'username': username})
        if isUser:
            flash("This username is already taken!")

        elif not isUser:
            hVal = hash_password(password)
            
            userdb.users.insert_one({'username': username, 'password': hVal})
            new_user = userdb.users.find_one({'username': username})
            return redirect(url_for('index'))

    flash("Please create a username and password")    
    return render_template('registration.html', form=form)

@app.route('/api/token', methods=['GET', 'POST'])
def login():
    form = LoginForm(request.form)
    username = form.username.data
    password = form.password.data
    remember = True
    
    if request.method == 'POST' and form.validate_on_submit():
        
        isUser = userdb.users.find_one({'username': username})
        if isUser:
            isUserHval= isUser['password']
            if pwd_context.verify(password, isUserHval):
                isUserID = str(isUser['_id'])
                isUsertest = User(str(isUser['_id']))
                token = generate_auth_token(expiration=600)
                token = token.decode()
                next = request.args.get("next")
                if next != None:
                    return redirect(next)
                else:
                    login_user(isUsertest, remember=remember)
                    return jsonify({"Token": token, "duration":600, "ID": isUserID})
            
            elif not pwd_context.verify(password, isUserHval):
                flash("The password you entered is wrong")
                return render_template('token.html', form=form)
            
        elif not isUser:
            flash("The username you entered is not found")
            return render_template('token.html', form=form)

        
    flash("Please Log in with your username and password")
    return render_template('token.html', form=form)

@app.route('/logout')
@login_required
def logout():
	logout_user()
	return render_template(token.html)
    
class listAll(Resource):
    @login_required
    def get(self):

        # Getting the top var from the URI
        top_nums = request.args.get('top')
        # if the var top was not in URI
        if top_nums == None:
            # Getting the data from MongoDB
            _items = db.tododb.find()
            items = [item for item in _items]
        # Else, if Var top is in URI
        else:
            # Saving the num in an integer form
            top_nums = int(top_nums)
            # Getting and sorting data and limiting the number of data requested from MongoDB
            _items = db.tododb.find().sort([("open_time",pymongo.ASCENDING), ("close_time",pymongo.ASCENDING)]).limit(top_nums)
            items = [item for item in _items]
            
        # A list to append the data from MongoDB
        open_time_list = []
        close_time_list = []

        # Appending the data to the lists
        for data in items:
            open_time_list.append(data['open_time'])
            close_time_list.append(data['close_time'])

        # Returning the final results    
        return {
            'open_time': open_time_list,
            'close_time': close_time_list
        }
    
# Create routes
# Another way, without decorators
api.add_resource(listAll, '/listAll')

        
class listOpenOnly(Resource):
    @login_required
    def get(self):
        # Getting the top var from the URI
        top_nums = request.args.get('top')
        # if the var top was not in URI
        if top_nums == None:
            # Getting the data from MongoDB
            _items = db.tododb.find()
            items = [item for item in _items]
            
        # Else, if Var top is in URI
        else:
            top_nums = int(top_nums)
            _items = db.tododb.find().sort([("open_time",pymongo.ASCENDING)]).limit(top_nums)
            items = [item for item in _items]

        # A list to append the data from MongoDB    
        open_time_list = []

        # Appending the data to the lists 
        for data in items:
            open_time_list.append(data['open_time'])
                        
        # Returning the final results 
        return {
            'open_time': open_time_list,
        }

# Create routes
# Another way, without decorators
api.add_resource(listOpenOnly, '/listOpenOnly')

class listCloseOnly(Resource):
    @login_required
    def get(self):
	# Getting the top var from the URI
        top_nums = request.args.get('top')
	# if the var top was not in URI
        if top_nums == None:
            # Getting the data from MongoDB
            _items = db.tododb.find()
            items = [item for item in _items]

        # Else, if Var top is in URI
        else:
            top_nums = int(top_nums)
            _items = db.tododb.find().sort([("close_time",pymongo.ASCENDING)]).limit(top_nums)
            items = [item for item in _items]

        # A list to append the data from MongoDB
        close_time_list = []
	
        # Appending the data to the lists
        for data in items:
            close_time_list.append(data['open_time'])

        # Returning the final results 
        return {
            'close_time': close_time_list,
        }

# Create routes
# Another way, without decorators
api.add_resource(listCloseOnly, '/listCloseOnly')


class listAllJSON(Resource):
    @login_required
    def get(self):
        # Getting the top var from the URI
        top_nums = request.args.get('top')
        # if the var top was not in URI
        if top_nums == None:
            # Getting the data from MongoDB
            _items = db.tododb.find()
            items = [item for item in _items]
        # Else, if Var top is in URI
        else:
            # Saving the num in an integer form
            top_nums = int(top_nums)
            # Getting and sorting data and limiting the number of data requested from MongoDB
            _items = db.tododb.find().sort([("open_time",pymongo.ASCENDING), ("close_time",pymongo.ASCENDING)]).limit(top_nums)
            items = [item for item in _items]

        # A list to append the data from MongoDB
        open_time_list = []
        close_time_list = []

        # Appending the data to the lists
        for data in items:
            open_time_list.append(data['open_time'])
            close_time_list.append(data['close_time'])

        # Returning the final results
        return {
            'open_time': open_time_list,
            'close_time': close_time_list
        }

# Create routes
# Another way, without decorators
api.add_resource(listAllJSON, '/listAll/json')

class listOpenOnlyJSON(Resource):
    @login_required
    def get(self):
        # Getting the top var from the URI
        top_nums = request.args.get('top')
        # if the var top was not in URI
        if top_nums == None:
            # Getting the data from MongoDB
            _items = db.tododb.find()
            items = [item for item in _items]

        # Else, if Var top is in URI
        else:
            top_nums = int(top_nums)
            _items = db.tododb.find().sort([("open_time",pymongo.ASCENDING)]).limit(top_nums)
            items = [item for item in _items]

        # A list to append the data from MongoDB
        open_time_list = []

        # Appending the data to the lists
        for data in items:
            open_time_list.append(data['open_time'])

        # Returning the final results
        return {
            'open_time': open_time_list,
        }

# Create routes
# Another way, without decorators 
api.add_resource(listOpenOnlyJSON, '/listOpenOnly/json')


class listCloseOnlyJSON(Resource):
    @login_required
    def get(self):
        # Getting the top var from the URI
        top_nums = request.args.get('top')
        # if the var top was not in URI
        if top_nums == None:
            # Getting the data from MongoDB
            _items = db.tododb.find()
            items = [item for item in _items]

        # Else, if Var top is in URI
        else:
            top_nums = int(top_nums)
            _items = db.tododb.find().sort([("close_time",pymongo.ASCENDING)]).limit(top_nums)
            items = [item for item in _items]

        # A list to append the data from MongoDB
        close_time_list = []
        # Appending the data to the lists
        for data in items:
            close_time_list.append(data['open_time'])

        # Returning the final results
        
        return {
            'close_time': close_time_list,
        }

# Create routes
# Another way, without decorators
api.add_resource(listCloseOnlyJSON, '/listCloseOnly/json')

class listAllCSV(Resource):
    @login_required
    def get(self):
        # Getting the top var from the URI
        top_nums = request.args.get('top')
        # if the var top was not in URI
        if top_nums == None:
            # Getting the data from MongoDB
            _items = db.tododb.find()
            items = [item for item in _items]
        # Else, if Var top is in URI
        else:
            # Saving the num in an integer form
            top_nums = int(top_nums)
            # Getting and sorting data and limiting the number of data requested from MongoDB
            _items = db.tododb.find().sort([("open_time",pymongo.ASCENDING), ("close_time",pymongo.ASCENDING)]).limit(top_nums)
            items = [item for item in _items]

        csvfile = open('data.csv', 'w')
        csv_writer = csv.writer(csvfile)
        csv_writer.writerow(['Open_Time', 'Close_Time'])

        for data in items:
            csv_writer.writerow([data['open_time'], data['close_time']])
        # Returning the final results
        csvfile = open('data.csv', 'r')
    
        return Response(csvfile, mimetype='text/csv')

# Create routes
# Another way, without decorators
api.add_resource(listAllCSV, '/listAll/csv')

class listOpenOnlyCSV(Resource):
    @login_required
    def get(self):
        # Getting the top var from the URI
        top_nums = request.args.get('top')
        # if the var top was not in URI
        if top_nums == None:
            # Getting the data from MongoDB
            _items = db.tododb.find()
            items = [item for item in _items]
        # Else, if Var top is in URI
        else:
            # Saving the num in an integer form
            top_nums = int(top_nums)
            # Getting and sorting data and limiting the number of data requested from MongoDB
            _items = db.tododb.find().sort([("open_time",pymongo.ASCENDING)]).limit(top_nums)
            items = [item for item in _items]

        csvfile = open('data.csv', 'w')
        csv_writer = csv.writer(csvfile)
        csv_writer.writerow(['Open_Time'])

        for data in items:
            csv_writer.writerow([data['open_time']])
        # Returning the final results
        csvfile = open('data.csv', 'r')

        return Response(csvfile, mimetype='text/csv')

# Create routes
# Another way, without decorators 
api.add_resource(listOpenOnlyCSV, '/listOpenOnly/csv')

class listCloseOnlyCSV(Resource):
    @login_required
    def get(self):
        # Getting the top var from the URI
        top_nums = request.args.get('top')
        # if the var top was not in URI
        if top_nums == None:
            # Getting the data from MongoDB
            _items = db.tododb.find()
            items = [item for item in _items]
        # Else, if Var top is in URI
        else:
            # Saving the num in an integer form
            top_nums = int(top_nums)
            # Getting and sorting data and limiting the number of data requested from MongoDB
            _items = db.tododb.find().sort([("close_time",pymongo.ASCENDING)]).limit(top_nums)
            items = [item for item in _items]

        csvfile = open('data.csv', 'w')
        csv_writer = csv.writer(csvfile)
        csv_writer.writerow(['Close_Time'])

        for data in items:
            csv_writer.writerow([data['close_time']])
        # Returning the final results
        csvfile = open('data.csv', 'r')

        return Response(csvfile, mimetype='text/csv')

# Create routes
# Another way, without decorators 
api.add_resource(listCloseOnlyCSV, '/listCloseOnly/csv')


# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
