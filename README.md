# Project 7
# Majed Almazrouei - malmazr2@uoregon.edu

----------------------------------
# Very Important to run the program.
# PUT the creditinial file inside DockerRestAPI/DockerMongo
# Use the run shell in the DockerRestAPI folder "$ bash run.sh "

#The project is about listing service from project 5 stored in MongoDB database using API rest.
# Also designed a consumer page using php.


# Ports to access the servers
    * localhost:5003 - access the ACP calculator page
    * localhost:5001 - access the APIs pages	
    * localhost:5000 - access the php website


# Functionality i added: 

    * "http://<host:port>/listAll" should return all open and close times in the database
    * "http://<host:port>/listOpenOnly" should return open times only
    * "http://<host:port>/listCloseOnly" should return close times only

Part 1: Authenticating the services
POST /api/register
Registers a new user. On success a status code 201 is returned. The body of the response contains a JSON object with the newly added user. A Location header contains the URI of the new user. On failure status code 400 (bad request) is returned. Note: The password is hashed before it is stored in the database. Once hashed, the original password is discarded. Your database should have three fields: id (unique index), username and password for storing the credentials.

GET /api/token
Returns a token. This request must be authenticated using a HTTP Basic Authentication (see password.py for example). On success a JSON object is returned with a field token set to the authentication token for the user and a field duration set to the (approximate) number of seconds the token is valid. On failure status code 401 (unauthorized) is returned.

GET /RESOURCE-YOU-CREATED-IN-PROJECT-6
Return a protected <resource>, which is basically what you created in project 6. This request must be authenticated using token-based authentication only (see testToken.py). HTTP password-based (basic) authentication is not allowed. On success a JSON object with data for the authenticated user is returned. On failure status code 401 (unauthorized) is returned.

Part 2: User interface


* Designed two different representations: one in csv and one in json.
    * "http://<host:port>/listAll/csv" should return all open and close times in CSV format
    * "http://<host:port>/listOpenOnly/csv" should return open times only in CSV format
    * "http://<host:port>/listCloseOnly/csv" should return close times only in CSV format

    * "http://<host:port>/listAll/json" should return all open and close times in JSON format
    * "http://<host:port>/listOpenOnly/json" should return open times only in JSON format
    * "http://<host:port>/listCloseOnly/json" should return close times only in JSON format

* Added a query parameter to get top "k" open and close times.

    * "http://<host:port>/listOpenOnly/csv?top=3" should return top 3 open times only (in ascending order) in CSV format 
    * "http://<host:port>/listOpenOnly/json?top=5" should return top 5 open times only (in ascending order) in JSON format
    * "http://<host:port>/listCloseOnly/csv?top=6" should return top 5 close times only (in ascending order) in CSV format
    * "http://<host:port>/listCloseOnly/json?top=4" should return top 4 close times only (in ascending order) in JSON format
